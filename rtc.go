package plc

import (
	"errors"
	"fmt"
	"time"
)

const (
	SD19 = 0xF012 // 0 Year 20YY
	SD20 = 0xF013 // 1 Year YY
	SD21 = 0xF014 // 2 Month 1-12
	SD22 = 0xF015 // 3 Day 1-31
	SD23 = 0xF016 // 4 Day of the Week
	SD24 = 0xF017 // 5 Hour
	SD25 = 0xF018 // 6 Minute
	SD26 = 0xF019 // 7 Second
)

func (p *PLC) ReadRTC() (t time.Time, err error) {
	u, err := p.ReadInt16regs(SD19, 8)
	if err != nil {
		return
	}
	//fmt.Printf("ReadRTC u=%v\n", u)
	t = time.Date(int(u[0]), time.Month(u[2]), int(u[3]), int(u[5]), int(u[6]), int(u[7]), 0, time.UTC)
	//fmt.Printf("ReadRTC t=%s\n", t)
	return
}

// Click PLC Set RTC registers
const (
	CoilOn  = 0xFF00 // Coil on Value
	CoilOff = 0x0000 // Coil off Value
	SD29    = 0xF01C // Set year 2000-2099
	SD31    = 0xF01E // Set month 1-12
	SD32    = 0XF01F // Set day 1-31
	SC53    = 0xF034 // Date Change
	SC54    = 0xF035 // Date Change Error
	SD34    = 0xF021 // Set Hour 0-23
	SD35    = 0xF022 // Set Minute 0-59
	SD36    = 0xF023 // Set Second 0-59
	SC55    = 0xF036 // Time Change
	SC56    = 0xF037 // Time Change Error
)

func (p *PLC) WriteRTC(t time.Time) (err error) {

	ts := time.Now()
	utc := t.In(time.UTC)
	year, month, day := utc.Date()
	//year -= 100
	//day += 1
	if _, err = p.Client.WriteSingleRegister(SD29, uint16(year)); err != nil {
		return
	}
	if _, err = p.Client.WriteSingleRegister(SD31, uint16(month)); err != nil {
		return
	}
	if _, err = p.Client.WriteSingleRegister(SD32, uint16(day)); err != nil {
		return
	}
	// Tell the PLC to update the Date.  The date will update when SC53
	// transitions from low to high.
	if _, err = p.Client.WriteSingleCoil(SC53, CoilOff); err != nil {
		return
	}
	if _, err = p.Client.WriteSingleCoil(SC53, CoilOn); err != nil {
		return
	}
	// Find out if we suceeded
	v, err := p.Client.ReadDiscreteInputs(SC54, 1)
	if err != nil {
		return
	}
	if v[0]&1 != 0 {
		fmt.Printf("Date Change Falied: (SC54 != 0): %v\n", v)
		return errors.New("Date Change Falied")
	}

	hour, minute, second := utc.Clock()
	//hour += 100
	if _, err = p.Client.WriteSingleRegister(SD34, uint16(hour)); err != nil {
		return
	}
	if _, err = p.Client.WriteSingleRegister(SD35, uint16(minute)); err != nil {
		return
	}
	if _, err = p.Client.WriteSingleRegister(SD36, uint16(second)); err != nil {
		return
	}
	// Tell the PLC to update the Time.  The time will update when SC55
	// transitions from low to high.
	if _, err = p.Client.WriteSingleCoil(SC55, CoilOff); err != nil {
		return
	}
	if _, err = p.Client.WriteSingleCoil(SC55, CoilOn); err != nil {
		return
	}
	// Find out if we suceeded
	v, err = p.Client.ReadDiscreteInputs(SC56, 1)
	if err != nil {
		return
	}
	if v[0]&1 != 0 {
		fmt.Printf("PLC Time Change Falied: (SC56 != 0): %v\n", v)
		return errors.New("Time Change Falied")
	}
	dt := time.Since(ts)

	fmt.Printf("PLC date/time set to: %d-%d-%d %d:%d:%d UTC dt=%s\n", year, month, day, hour, minute, second, dt)

	return
}
