package plc

import (
	"fmt"
	"time"
)

func (plc *PLC) TestPoll() (err error) {

	err = plc.handler.Connect()
	if err != nil {
		fmt.Printf("PLC: %v\n", err)
		return
	}
	defer plc.handler.Close()

	fmt.Printf("Connected to PLC at %s\n", plc.Address)

	plctime, err := plc.ReadRTC()
	if err != nil {
		return
	}
	systime := time.Now()
	fmt.Printf("Local time: %s\n", systime)
	fmt.Printf("PLC   time: %s\n", plctime)
	fmt.Printf("SYS   time: %s\n", systime.In(time.UTC))
	tdiff := systime.Sub(plctime)
	fmt.Printf("SYS-PLC time: %s\n", tdiff)

	err = plc.WriteRTC(time.Now())
	if err != nil {
		fmt.Printf("WriteRTC Failed: %v\n", err)
		return
	}

	ticker100ms := time.NewTicker(time.Millisecond * 100)
	ticker10s := time.NewTicker(time.Second * 10)

	for {
		select {
		case <-ticker100ms.C:
			ts := time.Now()
			vec, err := plc.ReadInt32regs(0x4000, 11)
			dt := time.Since(ts)
			if err != nil {
				return err
			}
			fmt.Printf("dt=%s vec=%v\n", dt, vec)

			fvec, _ := plc.ReadFloat32regs(DFbase+0, 7)
			fmt.Printf("fvec=%v\n", fvec)

		case <-ticker10s.C:
			plctime, err := plc.ReadRTC()
			if err != nil {
				break
			}
			fmt.Printf("PLC Time: %s\n", plctime)
		}
	}
}

func TestMain() {
	var plc *PLC
	//var plcaddress string = "10.9.8.169:502"
	var plcaddress string = "pi0:5556"
	plc = New(plcaddress)
	go func() {
		for {
			plc.TestPoll()
			fmt.Printf("PLC disconnected.  Retry in 2 seconds.\n")
			time.Sleep(2 * time.Second)
		}
	}()

	for {
		time.Sleep(time.Second)
		fmt.Println("*******************")
	}

}
